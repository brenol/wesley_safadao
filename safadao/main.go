package main

import (
	"net/http"
	"os"
)

func index(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(template))
}

func main() {
	port := os.Getenv("PORT")
	http.HandleFunc("/", index)
	http.ListenAndServe(":"+port, nil)
}

const template = `
<html>
        <head>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <style type="text/css">
                body {
                    margin:40px auto;
                    max-width:650px;
                    line-height:1.6;
                    font-size:18px;
                    color:#444;
                    padding:0 10px;
                    background: white;
                }
                h1,h2,h3 {
                    line-height:1.2
                }
            </style>
        </head>
        <body>
		<form id="safadao" name="safadao" onsubmit="wesley_safadao();return false;">
			<h3>Dia</h4>
			<input type="number" id="dia" placeholder="06" max="31" min="1" required>
			<h3>Mes</h4>
			<input type="number" id="mes" placeholder="09" max="12" min="0" required>
			<h3>Ano</h4>
			<input type="number" id="ano" placeholder="1988" min="0" required>
			<button type="submit">Calcular Safadeza!</button>
		</form>
        <div id="wesley"></div>
        
        <script language="javascript">
            function wesley_safadao() {
                var ano = document.getElementById("ano").value;
                var mes = document.getElementById("mes").value;
                var dia = document.getElementById("dia").value;
                
                safadeza = (mes + (ano/100) * (50-dia)) / 100;
                anjo = 100 - safadeza;
                if (safadeza > 100) {
                    safadeza = 100;
                    anjo = 0;
                }
                var doc = document.getElementById("wesley");
                doc.innerHTML = "<h3>Sua porcentagem de safadeza é " + safadeza + "%.<br>Sua porcentagem de anjo é " + anjo + "%.</h3><br>";
                
                if (anjo >= 90) {
                    doc.innerHTML += "Tem jeito não irmão, você nunca será safadão!";
                } else if (safadeza >= 90) {
                    doc.innerHTML += "Safadão demais. Nem Wesley aguenta.";
                } else if (safadeza >= 70) {
                    doc.innerHTML += "Você é muito safadão!";
                } else if (anjo >= 70) {
                    doc.innerHMTL += "Ainda bem que você não é mais anjo que isso! Já imaginou?";
                } else {
                    doc.innerHTML += "Você é um safadão normal.";
                }
                 
            }
        </script>
         
            <div style="position: auto; bottom: 5px;">
            <hr>
                Filhos de Fábio agradece sua preferência.
                <br>
                POWERED BY BRENO SAFADÃO
            </div>
    
        </body>
    </html>
`
